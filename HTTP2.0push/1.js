/**
 * 最简SPDY服务
 * User: yourtion
 */
var spdy = require("spdy"),
    options = "";
spdy.createServer(options,function(req,res){
    // push JavaScript asset (/main.js) to the client
    res.push('/main.js',{'content-type':'application/javascript'},function(err,stream){
        stream.end('alert("Hello from Push Stream!"');
    });
    // write main response body and terminate stream
    res.end('Hello World! <script src="/main.js"></script>');
}).listen(443);