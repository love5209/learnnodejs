/**
 * Module dependencies.
 */

var express = require('express'),
routes = require('./routes'),
user = require('./routes/user'),
http = require('http'),
fs = require('fs'),
exec = require('child_process').exec,
path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3002);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'upload')));
app.use(express.bodyParser({
  keepExtensions: true,
  uploadDir: '/upload'
}));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

function reSize(filename, callback){
  console.log('Start reSize');
  console.log(filename);
  var ident = exec('convert -resize x1024 ' + filename + ' ./temp/resize.png',
  function(error, stdout, stderr) {
    console.log('ident');
    console.log('stdout: ' + stdout);
    console.log('stderr: ' + stderr);
    if (error !== null) {
      console.log('exec error: ' + error);
    }
    console.log('ident OK');
    callback('./temp/resize.png');
  });
}
function compoCover(filename, callback){
  console.log('pCoverLeft');
  var ident = exec('composite -gravity northwest ./yy.png '+filename+' ./temp/coverOK.png',
  function(error, stdout, stderr) {
    console.log('ident');
    console.log('stdout: ' + stdout);
    console.log('stderr: ' + stderr);
    if (error !== null) {
      console.log('exec error: ' + error);
    }
    console.log('ident OK');
    callback('./temp/coverOK.png');
  });
}
function pCoverLeft(filename, callback){
  console.log('pCoverLeft');
  var ident = exec('convert '+ filename +' -gravity southwest -crop 31x1024+0+0 ./upload/left.png',
  function(error, stdout, stderr) {
    console.log('ident');
    console.log('stdout: ' + stdout);
    console.log('stderr: ' + stderr);
    if (error !== null) {
      console.log('exec error: ' + error);
    }
    console.log('ident OK');
    callback('./temp/coverOK.png');
  });
}
function pCoverRight(filename, callback){
  console.log('pCoverLeft');
  var ident = exec(' convert '+filename+' -gravity southeast -crop 737x1024+0+0 ./upload/right.png',
  function(error, stdout, stderr) {
    console.log('ident');
    console.log('stdout: ' + stdout);
    console.log('stderr: ' + stderr);
    if (error !== null) {
      console.log('exec error: ' + error);
    }
    console.log('ident OK');
    callback('OK');
  });
}


app.get('/', routes.index);
app.post('/upload',
function(req, res) {
  // 获得文件的临时路径
  var date = new Date();
  var tmp_path = req.files.imgs.path;
  console.log(tmp_path);
  // 指定文件上传后的目录 - 示例为"pdf"目录。
  var target_path = './upload/' + req.files.imgs.name;
  console.log(target_path);
  // 移动文件
  fs.rename(tmp_path, target_path,
  function(err) {
    if (err) throw err;
    // 删除临时文件夹文件,
    fs.unlink(tmp_path,
    function() {
      if (err) throw err;
      //跳转到转换文件
      console.log(req.files.imgs.name);
      var s1=new Date();
      reSize(target_path,function (data){
        compoCover(data,function(data){
          pCoverLeft(data,function(data){
            pCoverRight(data,function(data){
              var s2 = new Date();
              console.log(s2 - s1 + "ms");
              res.redirect("/" + req.files.imgs.name);
            });
          });
        });
      });
    });
  });
});
http.createServer(app).listen(app.get('port'),
function() {
  console.log('Express server listening on port ' + app.get('port'));
});
