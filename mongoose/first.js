var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.connect('mongodb://localhost/test');

var blogSchema = new Schema({
    title: String,
    author: String,
    body: String,
    comments: [{body: String, date: Date }],
    date: {type: Date, default: Date.now},
    hidden: Boolean,
    meta: {
        votes: Number,
        favs: Number
    }
});

var Blog = mongoose.model('Blog', blogSchema);
//Blog.create({
//    title: 'My first Blog',
//    author: 'Yourtion',
//    body: 'my first Node mongoose test blog!'
//},function(err ,small){
//    if(err) return handler(err);
//    console.log('Save!');
//})
Blog.find({author:'Yourtion'},function(data){
    console.log(data);
});
Blog.remove({ author: 'me' }, function (err) {
    if (err) return handleError(err);
    console.log('Remove!');
});

