/**
 * Module dependencies.
 */

var express = require('express')
    , routes = require('./routes')
    , user = require('./routes/user')
    , blogs = require('./routes/blogs')
    , error = require('./routes/error')
    , http = require('http')
    , path = require('path')
    , fs = require('fs')
    , markdown = require('markdown-js');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);
// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.engine('md', function (path, options, fn) {
    fs.readFile(path, 'utf8', function(err, str){
        if (err) return fn(err);
        str = markdown.parse(str).toString();
        fn(null, str);
    });
});

app.get('/', routes.index);
app.get('/users/:id', user.list);
app.get('/users/*', user.show);
app.get('/markdown', function (req, res) {
    res.render('index.md', {layout: false});
});
app.get('/blogs/:title.html', blogs.show);
app.get('*', error.notfound);

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
