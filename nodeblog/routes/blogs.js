
/*
 * GET home page.
 */
var path = require('path');
exports.show = function(req, res, next){
    var urlpath = [
        process.cwd(),
        '/views/blogs/',
        req.params.title,'.md'
    ].join('');
    console.log(urlpath);
    var filepath = path.normalize(urlpath);
    console.log(filepath);
    path.exists(filepath, function(exists){
        if(!exists){
            console.log("next");
            next();
        }else{
            res.render(urlpath,{layout:false});
        }
    })
};