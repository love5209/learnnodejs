/*
运行这段代码，1秒后控制台输出了 some_event occured.。
其原理是 event 对象注册了事件 some_event 的一个监听器，
然后我们通过 setTimeout 在1000毫秒以后向event 对象发送事件 some_event，
此时会调用 some_event 的监听器。
*/
var EventEmitter=require('events').EventEmitter;
var event=new EventEmitter;

//注册事件监听器“some_event”
event.on('some_event',function(){
	console.log('some_event occured.');
});

//延时一秒
setTimeout(function(){
	//发生事件“some_evert”
	event.emit('some_event');
},1000);

