var fs = require("fs");

/*
异步式读取文件end.先被输出。异步式 I/O 是通过回调函数来实现的。
fs.readFile 接收了三个参数，第一个是文件名，第二个是编码方式，第三个是一个函数，
我们称这个函数为回调函数。JavaScript 支持匿名的函数定义方式，
例子中回调函数的定义就是嵌套在fs.readFile 的参数表中的。
*/
//异步读取
console.log("异步读取开始！");
fs.readFile('file.txt','utf-8',function(err,data){
	if(err){
		console.error(err);
	} else {
		console.log('异步结果：'+data);
	}
});
console.log('异步读取执行结束。');

/*
同步式读取文件的方式比较容易理解，将文件名作为参数传入 fs.readFileSync 函数，
阻塞等待读取完成后，将文件的内容作为函数的返回值赋给 data 变量，
接下来控制台输出 data 的值，最后输出 end.。
*/
//同步读取
console.log("同步读取开始！");
var data=fs.readFileSync('file.txt','utf-8');
console.log('同步结果：'+data);
console.log('同步读取执行结束。');

//异步读取CallBack方法
console.log("异步读取CallBack开始！");
function readFileCallBack(err,data){
	if(err){
		console.error(err);
	} else {
		console.log('异步CallBack结果：'+data);
	}
}
fs.readFile('file.txt','utf-8',readFileCallBack);
console.log('异步读取CallBack结束');